==Description==

This animated gif file shows parabolic Julia set called Cauliflower. 

Gif consist of 26 frames numbered from 0 to 25 with the number of iterations ( n). 


=== making animated gif ===
* first find exterior ( escaping set) and filled-in julia set ( non-escaping or bounded set) using simple escape time ( see ComputeFatouComponents function in c src code). This image is saved in data array. It is common for all frames and this image is not saved, but one can do it by uncomment SaveArray2PGMFile( data,999); line  
* then make all pgm images ( see ComputeColorZero funtion in C src code  and zero array)
* convert pgm images to frames of animated gif using bash src code




===Frames and plane ===
Each frame of animated gif shows rectangle part of the dynamic plane : 

<source lang=c>
/* world ( double) coordinate = dynamic plane */
static   const double ZxMin=-1.5;
static  const double ZxMax=1.5;
static  const double ZyMin=-1.5;
static  const double ZyMax=1.5;
</source>

for different number of iterations ( n) which is displayed in left upper corner

===Arrays ===
There are 3 arrays : 
* data for main components of dynamic plane ( escaping and non-escaping set). See escape time algorithm and  ComputeColorZero funcion
* edge for boundaries of components : Julia set, but also for boundaries between other components. See Sobel filter and ComputeBoundariesIn function 
* zero for components of Zero algorithm. See ComputeColorZero function


<source lang=c>
// memmory 1D array 
unsigned char *data;
unsigned char *edge;
unsigned char *zero;
</source>


Each array can be : 
* saved as a pgm image ( see SaveArray2PGMFile function)
* copied to other array ( see CopyBoundariesTo function ) 
* used to save information about colour of pixels ( shades of gray = char = integer from 0 to 255 )

===Colouring algorithms===

There are 3 colouring algorithms:
* [[:wikibooks:Fractals/Iterations_in_the_complex_plane/Julia_set#Boolean_Escape_time|boolean escape time]]. See ComputeColorZero function
* [[:wikibooks:Fractals/Computer_graphic_techniques/2D#Edge_detection|sobel filter for finding boundaries = edge detection]]  ( like Julia set, but also for other boundaries ). See  ComputeBoundariesIn function.
* zero algorithm. See ComputeColorZero function.

Program flow : 
* ComputeZerosOfQnc function calls PlotPointZero function for each element of the array zero
* function PlotPointZero calls ComputeColorZero function and computes colour of each pixel
* Zero algorithm is implemented in the ComputeColorZero function


ComputeColorZero function :
* takes as the input : 
** integer coordinate of the point ( ix, iy)
** maximal number of iterations (n=iMax)
* converts integer to double coordinate of pixel ( from screen to world, here dynamic z-plane ) :  z = Zx+Zy*I
* makes (iMax) of iterations  iterations : zn = fn(z0)  
* checks zn and returns colour ( char number iColor )

<source lang=c>
// check position near fixed point after n iterations 
  if ( Zx>0 && Zy>0) iColor = 150; //re(z_n) > 0 and im(z_n) > 0 (first quadrant)
  if ( Zx<0 && Zy>0) iColor = 170; //re(z_n) < 0 and im(z_n) > 0 (second)
  if ( Zx<0 && Zy<0) iColor = 190; //re(z_n) < 0 and im(z_n) < 0 (third)
  if ( Zx>0 && Zy<0) iColor = 200; //re(z_n) > 0 and im(z_n) < 0 (fourth).
</source>

Note that in Zero algorithm :
* there is no [[Wikipedia:Fractals/Iterations_in_the_complex_plane/def_cqp#Bailout_test|escaping test ( bailout test)]]
* there is no attraction test 
* the number of iterations the same for all pixels

===the number of iterations ===

The maximal number of iterations is displayed in left upper corner of each frame. 

In the source code it is denoted by n variable : 

<source lang =c>
 int n; 
</source>


Note that for number of iterations ( n) : 
* n<10 both exterior ( escaping set)  and interior ( non-escaping = bounded set) is coloured with the same algorithm ( see ComputeColorZero funtion)
* n>=10 only interior is coloured with ComputeColorZero funcion. Exterior is coloured with simple solid color ( iColorOfExterior )
* number of details is proportional to n. See what happens near the cusps of Julia set, especially when N>=10 and all exterior has the same colour
See : 

<source lang=c>
iColor = ComputeColorZero(ix, iy, n);
  if (data[i]==iColorOfInterior) A[i] =  iColor+20; // interior 
     else {if (n<10) A[i]= iColor; else A[i]= iColorOfExterior;} // exterior , only for low n 
</source>
